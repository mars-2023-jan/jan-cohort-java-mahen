package com.mars.training;

public class Employee {
	
	// Instance Variables
	private String empName;
	
	private String empAddress;
	
	private double salary;
	
	private int empId;
	
	Employee(){
		System.out.println("Constructor called...");
	}
	
	Employee(String empName, double salary){
		this.empName = empName;
		this.salary = salary;
	}
		
	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}


	public String getEmpAddress() {
		return empAddress;
	}



	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	//static variable
	static String companyName;
	
	public String getEmpDetails() {
		return "Company Name: "+this.companyName+" Employee Name: "+this.empName+
				" Employee ID:"+this.empId+" Salary:"+this.salary;
	}

}

// Create 4 employee objects with different designations and different salaries
// display employee details of employee with minimum salary.
// 
