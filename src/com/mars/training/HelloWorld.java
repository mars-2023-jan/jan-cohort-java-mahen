package com.mars.training;

public class HelloWorld {
	
	public static void main(String args[]) {
		System.out.println("Hello World from JAVA!!");
		
		Employee.companyName = "Mars";
		Employee emp1 = new Employee("Peter", 200);
		emp1.setEmpId(101);
		//emp1.setEmpName("John");
		emp1.setEmpAddress("Chicago");
		//emp1.setSalary(10000);
		
		System.out.println(emp1.getEmpDetails());
	
	}
}

// What is JRE?
// What is a JDK?
// What is a JVM?

// 1. Core JAVA - JDBC
// 2. JAVA EE
// 3. Spring framework
// 3.1 Spring MVC
// 3.2 Spring boot
// 4. APIs using spring boot (JPA, Hibernate)