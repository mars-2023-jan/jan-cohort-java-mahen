package com.mars.training;

import java.util.Scanner;

public class DataDemo {

	public static void main(String[] args) {
		
		// Local variables
		byte b = 25;
		
		int i = b;  // implicit typecast
		
		short s = (short)i; //explicit typecast
		
		float f = 30.6f;
		
		Scanner sc = new Scanner(System.in);
		
		
		  System.out.println("Input first number:");
		  
		  int firstNumber = sc.nextInt();
		  
		  System.out.println("Input second number:");
		  
		  int secondNumber = sc.nextInt();
		 
		
		DataDemo demo1 = new DataDemo();
		
		int sum = demo1.add(firstNumber, secondNumber);
		
		System.out.println("Sum of two numbers is: "+sum);
		
		System.out.println("Enter your name:");
		
		String name = sc.nextLine();
		
		System.out.println("Name is: "+name);

	}
	
	private int add(int a, int b) {
		return a+b;
	}
	
	
}
